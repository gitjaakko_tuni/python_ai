# SCRIPT TO CHECK ALL REQUIRED MACHINE LEARNING DEPENDENCIES ARE INSTALLED
# theano
import theano
print('theano: %s' % theano.__version__)
# tensorflow
import tensorflow
print('tensorflow: %s' % tensorflow.__version__)
# keras
import keras
print('keras: %s' % keras.__version__)