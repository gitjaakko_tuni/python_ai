# Python_AI

Kehittyneet ohjelmointikielet -kurssin 1. projekti

Koneoppimisprojekti python kieltä käyttäen.

Tekijät: Jaakko Pullinen & Petri Pellinen

Linkki projektin esityksessä käytettyyn .ppt:

https://tuni-my.sharepoint.com/:p:/g/personal/jaakko_pullinen_tuni_fi/EUYvxtHxPwVCq_a3DZa4x2sBfBqoGW1YK_d2EQ56M5bv1A?e=J0wFoW

Linkki aiempaan esitykseemme Pythonista:

https://tuni-my.sharepoint.com/:p:/g/personal/jaakko_pullinen_tuni_fi/EWxQ95reTH9KtpBupgaxFQUB__QdnwRO-TUNcBnW_b1IBQ?e=S3vpJ5

**Ohjeet:**

**ASENNA ANACONDA**
https://www.anaconda.com/distribution/

*  -> Python 3.7 version


Lisäohjeita: https://docs.anaconda.com/anaconda/install/

**ALUSTA VIRTUAALIYMPÄRISTÖ**

1. Avaa Anaconda command prompt 
2. Luo uusi virtuaaliympäristö, anna nimi ja määritä käytettävä python versio(esim 3.7) `conda create --name myEnvName python=3.7`
3. Aktivoi tekemäsi virtuaaliympäristö `conda activate myEnvName`
4. Asenna virtuaaliympäristöön tarvittavat paketit (allaolevassa listassa) käyttämällä `conda install paketinNimi`
5. Asenna tensorflow virtuaaliympäristöön `pip install tensorflow`
*  Projektissa käytettävät paketit:
*  pandas
*  scipy
*  numpy
*  matplotlib
*  sklearn
*  theano
*  keras


Cheat sheet conda käskyille:
https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf

**AJA OHJELMA**
* `git clone https://gitlab.com/gitjaakko_tuni/python_ai.git && cd python_ai`
* Tarkista että sinulla on tarvittavat paketit asennettuna `cd scripts && python versions.py && python deep_versions.py && cd ..`
* Aja haluamasi toiminto src kansiosta `cd src` `python evaluation.py/plotting.py/predictions.py`


**JOS KÄYTÄT VS CODEA**

Python virtuaaliympäristöt vscodessa:
https://code.visualstudio.com/docs/python/environments